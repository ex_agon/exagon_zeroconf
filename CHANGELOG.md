# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0]

- Remove unused `:map_diff` dependency
- Add support for both IPv4 and IPv4 in `Exagon.Zeroconf.Mdns.Dnssd.Service` struct
- Add support for registering DNS-SD services

## [0.1.0]

### Added 

- First Multicast DNS responder implementation. Limitation: this implementation currently doesn't publish and
DNS record data.
- Implement DNS server discovery.