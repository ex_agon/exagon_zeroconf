# Copyright 2022 Exagon team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
defmodule Exagon.Zeroconf.Supervisor do
  @moduledoc """
  Exagon Zeoconf module supervisor. Starts and manages MDNS server and DNS-SD service discorery processes.
  """
  use Supervisor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    children = [
      {Phoenix.PubSub, name: :zeroconf_pubsub},
      Exagon.Zeroconf.Mdns.Server,
      Exagon.Zeroconf.Mdns.Dnssd
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
