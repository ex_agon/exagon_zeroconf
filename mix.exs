# Copyright 2022 Exagon team
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule ExagonZeroconf.MixProject do
  use Mix.Project

  def project do
    [
      app: :exagon_zeroconf,
      version: "0.2.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      name: "Exagon Zeroconf",
      source_url: "https://gitlab.com/ex_agon/exagon_zeroconf",
      homepage_url: "https://gitlab.com/ex_agon/exagon_zeroconf",
      description: "Exagon Zeroconf utilities module",
      package: [
        maintainers: ["Nicolas Jouanin"],
        licenses: ["Apache-2.0"],
        links: %{
          "Source" => "https://gitlab.com/ex_agon/exagon_zeroconf"
        }
      ],
      source_url: "https://gitlab.com/ex_agon/exagon_zeroconf",
      docs: [
        extras: ["README.md", "LICENSE"]
      ]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Exagon.Zeroconf.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.27", only: :dev, runtime: false},
      {:dns, "~> 2.4.0"},
      {:phoenix_pubsub, "~> 2.0"}
    ]
  end
end
