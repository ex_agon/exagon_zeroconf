import Config

config :exagon_zeroconf, :mdns,
  port: 5353,
  use_ipv4: true,
  use_ipv6: false,
  hostname: "exagon",
  domain_name: ".local"

config :logger, level: :info
